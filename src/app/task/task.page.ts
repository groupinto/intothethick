import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-task',
  templateUrl: './task.page.html',
  styleUrls: ['./task.page.scss'],
})
export class TaskPage implements OnInit {
  posts: unknown[];

  constructor(private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private firestore: AngularFirestore) { }

  ionViewWillEnter(){
    this.getPosts();
  }

  async getPosts(){
    let loader =this.loadingCtrl.create({
      message: "Please wait...",
      duration: 200
    });
    (await loader).present();

    try{
       this.firestore
       .collection("posts")
       .snapshotChanges()
       .subscribe(data =>{
         this.posts =data.map(e=>{
           return{
             id: e.payload.doc.id,
             title: e.payload.doc.data()["title"],
             details: e.payload.doc.data()["details"]
           };
         });
       });
  
      }

     

    catch(e){
       this.showToast(e);
    }
  }

  async deletePost(id: string){

    let loader =this.loadingCtrl.create({
      message: "Please wait..."
    });
    (await loader).present();

    await this.firestore.doc("posts/" +id).delete();
    (await loader).dismiss();
  }

  showToast(message: string) {
    this.toastCtrl
    .create({
      message: message,
      duration: 2000
    })
    .then(toastData => toastData.present());
  }

  ngOnInit() {
  }

}

import { Component, OnInit } from '@angular/core';
import {  NavController, ToastController } from '@ionic/angular';
import {Post} from "../model/post.mode";
import { LoadingController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.page.html',
  styleUrls: ['./add-post.page.scss'],
})
export class AddPostPage implements OnInit {
 post={} as Post;
  constructor( private toastCtrl: ToastController,
     private loadingController: LoadingController,
     private navCtrl: NavController,
     private firestore: AngularFirestore
     ) { }

  ngOnInit() {
  }

    async createPost(post: Post){
      if(this.formValidation()){
        const loading = await this.loadingController.create();
        await loading.present();
        try{
          await this.firestore.collection("posts").add(post);
        }
        catch(e){
         this.showToast(e);
        }
        loading.dismiss();

        this.navCtrl.navigateRoot("collab1");
      }
    }

    formValidation(){
      if (!this.post.title){
        this.showToast("Enter Task");
        return false;
      }
   
     return true;
    }

    showToast(message: string){
      this.toastCtrl
      .create({
        message: message,
        duration:3000
      })
      .then(toastData => toastData.present());
    }

}

import { Component } from '@angular/core';
import { AnimationController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  myCustomPageTransition=((baseEl: any,opts?: any) =>{
    console.log("opt.enteringEl:"+ opts.enteringEl);
    console.log("opt.leavingEl:"+ opts.leavingEl);
    var anim1=this.animationCtrl.create()
    .addElement(opts.leavingEl)
    .duration(1500)
    .iterations(1)
    .fromTo('transform', 'translateX(0px)', 'translateX(700px)')
    .fromTo('opacity', '1', '0.2');
    var anim2=this.animationCtrl.create()
    .addElement(opts.enteringEl)
    .duration(500)
    .iterations(1)
    .easing('ease-out')
    .fromTo('opacity','0','1')
    var anim2=this.animationCtrl.create()
    .duration(500)
    .iterations(1)
    .addAnimation([anim1,anim2]);
   
    return anim2;

  });
  constructor(private animationCtrl: AnimationController) {}
}

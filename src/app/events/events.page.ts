import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from '../services/chat.service';
import { AlertController, LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-events',
  templateUrl: './events.page.html',
  styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit {


  constructor(private chatService: ChatService, private router: Router,    private alertController: AlertController,) { }
  async signOut() {
    const toast = await this. alertController.create({
      message:'Logout successfully',
   
    
    });
 
    toast.present();
    this.chatService.signOut().then(() => {
      this.router.navigateByUrl('/log', { replaceUrl: true });
      
    });
    
  }
  goBack(){
    this.router.navigate(['home']);
  }
  option = {
    slidesPerView: 1.5,
    centeredSlides: true,
    loop: true,
    spaceBetween: 10,
    autoplay:true,
  }
  
  ngOnInit() {
  }
async presentAlert(){
    const alert = await this.alertController.create({
      header: 'Hello, there!',
      message: 'Are you sure that you want to proceed?',
      buttons: ['Cancel','Yes']
    });

    await alert.present();

  }
}

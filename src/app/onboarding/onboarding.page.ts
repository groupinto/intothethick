import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlide, IonSlides } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.page.html',
  styleUrls: ['./onboarding.page.scss'],
})
export class OnboardingPage implements OnInit {
  public onboardSlides = [];
  @ViewChild('mainSlides',{static: true}) slides: IonSlides;
  constructor(private router: Router,) { }

  ngOnInit() {
    this.onboardSlides = [
      {
        title:'track your progress',
        img: 'first',
        desc: 'Leave your highs and lows in a digital journal to build a space for creativity. '
      },
      {
        title:'educate yourself',
        img: 'second',
        desc: 'Learn to appreciate life more.'
      },
      {
        title:'engage in a community',
        img: 'third',
        desc: 'Let your voice be heard.'
      }
    ];
  }

    public goBack(){
      this.slides.slidePrev();
    }

    skipBtn(){
      this.router.navigate(['home']);
    }

    public goNext(){
      this.slides.slideNext();
    }
}

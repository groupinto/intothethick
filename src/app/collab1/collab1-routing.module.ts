import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Collab1Page } from './collab1.page';

const routes: Routes = [
  {
    path: '',
    component: Collab1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Collab1PageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Collab1PageRoutingModule } from './collab1-routing.module';

import { Collab1Page } from './collab1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Collab1PageRoutingModule
  ],
  declarations: [Collab1Page]
})
export class Collab1PageModule {}

import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Post } from '../model/post.mode';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.page.html',
  styleUrls: ['./edit-post.page.scss'],
})
export class EditPostPage implements OnInit {
post = {} as Post;
id:any;

  constructor(private toastCtrl: ToastController,private actRoute: ActivatedRoute, private loadingController: LoadingController, private firestore: AngularFirestore, private navCtrl: NavController) { 

    this.id=this.actRoute.snapshot.paramMap.get("id");
  }
     
  ngOnInit() {
    this.getPostById(this.id);
  }
  async getPostById(id: string){
      const loading = await this.loadingController.create();
      await loading.present();

      this.firestore.doc("posts/" +id)
      .valueChanges()
      .subscribe(data=>{ 
        this.post.title = data["title"];
        this.post.details= data["details"];
      });
      loading.dismiss();

   
    }

    async updatePost(post: Post){
      if(this.formValidation()){
       
        try{
          await this.firestore.doc("posts/" + this.id).update(post);
        }
        catch(e){
         this.showToast(e);
        }


        this.navCtrl.navigateRoot("task");
      }
    }

    formValidation(){
      if (!this.post.title){
        this.showToast("Enter Task");
        return false;
      }
     
     return true;
    }

    showToast(message: string){
      this.toastCtrl
      .create({
        message: message,
        duration:3000
      })
      .then(toastData => toastData.present());
    }


  }



import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from '../services/chat.service';
import { AlertController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-community',
  templateUrl: './community.page.html',
  styleUrls: ['./community.page.scss'],
})
export class CommunityPage implements OnInit {
  tweets = [];
  segment = 'home';
  opts = {
    slidesPerView: 4.5,
    spaceBetween: 10,
    slidesOffsetBefore: 0
  };

  constructor(private http: HttpClient, private chatService: ChatService, private router: Router,    private alertController: AlertController,) {}
  
  addStory(){
    this.router.navigate(['story']);
  }

  async signOut() {
    const toast = await this. alertController.create({
      message:'Logout successfully',
   
    
    });
 
    toast.present();
    this.chatService.signOut().then(() => {
      this.router.navigateByUrl('/log', { replaceUrl: true });
      
    });
    
  }

  goBack(){
    this.router.navigate(['events']);
  }

  ngOnInit() {
    this.http.get('https://devdactic.fra1.digitaloceanspaces.com/twitter-ui/tweets.json').subscribe((data: any) => {
      console.log('tweets: ', data.tweets);
      this.tweets = data.tweets;
    });
  }
}
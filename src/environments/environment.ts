// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false.valueOf,
  firebaseConfig : {
    apiKey: "AIzaSyA7E2VfLUhBrauShYv1RxHxbepI9oPgrrI",
    authDomain: "fir-login-49478.firebaseapp.com",
    projectId: "fir-login-49478",
    storageBucket: "fir-login-49478.appspot.com",
    messagingSenderId: "828388862368",
    appId: "1:828388862368:web:56107e2ebc30c701495cc5"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
